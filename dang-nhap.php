<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>
  <!-- end header nav -->
  <?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();

    if (postInput("ten_dang_nhap") == NULL) {
      $error['ten_dang_nhap'] = 'Tên đăng nhập không được trống';
    } else{
      $ten_dang_nhap = postInput("ten_dang_nhap");
    }

    if (postInput("mat_khau") == NULL) {
      $error['mat_khau'] = 'mật khẩu không được trống';
    } else {
      $mat_khau = md5(postInput("mat_khau"));
    }

    if (empty($error)) {
      $is_check = mysqli_fetch_assoc($db->query("SELECT * FROM chutro WHERE ten_dang_nhap	 = '$ten_dang_nhap' AND mat_khau = '$mat_khau'"));
      if (isset($is_check) != NULL) {
        $_SESSION['ten_chutro'] = $is_check['ho_ten'];
        $_SESSION['chutro_id'] = $is_check['ma_chu_tro'];
        $_SESSION['chutro_ten_dang_nhap'] = $is_check['ten_dang_nhap'];
        $_SESSION['chutro_sdt'] = $is_check['sdt'];

        echo "<script>alert('Đăng nhập thành công');location.href='index.php'</script>";

      } else {
        $_SESSION['error_login_customer'] = "Tài khoản hoặc mật khẩu không chính xác";

      }
    }

  }
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <h2 class="text text-success">Đăng nhập tài khoản</h2>
            <?php if(isset($_SESSION['error_login_customer'])) :?>
            <p class="alert alert-danger text-center">Tên đăng nhập hoặc mật khẩu không chính xác!</p>
            <?php endif ?>
            <div class="col-md-8 col-md-offset-2 login-page">
              <form class="form-horizontal" method="POST" action="">

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tên đăng nhập</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="ten_dang_nhap" class="form-control" id="username" placeholder="Tên đăng nhập ..." value="<?= old("ten_dang_nhap") ?>">
                    <?php
                    if (isset($error['ten_dang_nhap'])) echo "<span class='help-block'><span style='color:red;'>" . $error['ten_dang_nhap']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_password" class="col-sm-4 control-label">Mật khẩu</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="password" name="mat_khau" class="form-control" id="password" placeholder="Mật khẩu ..." value="<?= old("mat_khau") ?>">
                    <?php
                    if (isset($error['mat_khau'])) echo "<span class='help-block'><span style='color:red;'>" . $error['mat_khau']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-success">Đăng nhập</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php  include 'layouts/footer.php';?>

</body>
</html>
