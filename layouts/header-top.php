<div class="header-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="pull-left">
          <a class="logo" href="<?= base_url() ?>"><img class="img-responsive" src="<?= base_url() ?>public/images/logo.png" style="margin-left:20px"></a>
        </div>

        <div class="col-md-5">
          <!-- <div class="" class="form-search" >
            <form class="" action="<?= base_url() ?>tim-kiem.php" method="post" style="padding-top:10px">
              <div class="col-md-10">
                <div class="col-md-4">
                  <input class="form-control" type="text" name="giatu" value="" style="color:#000" placeholder="Giá từ" required>

                </div>
                <div class="col-md-4">
                  <input class="form-control" type="text" name="giaden" value="" style="color:#000" placeholder="Đến" required>
                </div>
                <div class="col-md-4">
                  <button type="submit" name="button" class="btn btn-primary">Tìm kiếm</button>

                </div>
              </div>

            </form>
          </div> -->
        </div>
        <div class="pull-right">
          <div class="header-top-avatar">
            <img class="img-responsive" src="<?= base_url() ?>public/images/avatar.png">

          </div>

          <?php if(!isset($_SESSION['ten_chutro'])) :?>
            <div class="header-top-user">
              <div class="user-login">
                <a class="load_modal_login" href="<?= base_url() ?>dang-nhap.php">Đăng nhập</a>
              </div>
              <div class="user-register">
                <a href="<?= base_url() ?>dang-ki-tai-khoan.php">Tạo tài khoản</a>
              </div>
            </div>
          <?php else: ?>
            <div class="header-top-user">
              <div class="user-name">
                <a href="<?= base_url() ?>thong-tin-tai-khoan.php?ma_chu_tro=<?= $_SESSION['chutro_id'] ?>"><?= $_SESSION['chutro_ten_dang_nhap'] ?></a>
              </div>
              <div class="dropdown user-info">
                <a href="javascript: void(0);" class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Thông tin tài khoản <b class="caret"></b></a>
                <ul class="dropdown-menu dropdown-menu-right menu-user-info" aria-labelledby="dropdownMenu1">
                  <li><a href="<?= base_url()?>thong-tin-tai-khoan.php?ma_chu_tro=<?= $_SESSION['chutro_id'] ?>">Trang cá nhân</a></li>
                  <li><a href="<?= base_url() ?>tin-da-dang.php">Tin đã đăng</a></li>
                  <li><a href="<?= base_url() ?>dang-xuat.php">Đăng xuất</a></li>
                </ul>
              </div>
            </div>
          <?php endif; ?>
        </div>
        <div class="pull-right">
          <a class="btn btn-submit-room load_modal_login" href="<?= base_url() ?>dang-tin.php">Đăng tin miễn phí</a>
        </div>
      </div>
    </div>
  </div>
</div>
