<div class="footer" style="margin-top:90px">
    <div class="container">
        <div class="row">
            <div class="col-md-12 footer-top">

                <div class="row block-footer-2">
                    <div class="col-xs-6 col-sm-6 col-md-2">
                        <h3>Thông tin liên hệ</h3>
                        <p>Tel : 0989.999.999<br>
                        <span>Email : support@suniostudio.com</span></p>
                        <!-- <img class="img-responsive" src="img/footer-logo.png" alt=""> -->
                    </div>
                
                    <div class="col-xs-6 col-sm-6 col-md-4 footer-about">
                        <h3>Về chúng tôi</h3>
                        <p>Trang web đăng tin chia sẻ miễn phí thông tin về phòng trọ, nhà riêng, văn phòng nhằm giúp người thuê phòng và người cho thuê phòng tương tác với nhau không qua trung gian môi giới.</p>
                    </div>

                </div>
                <div class="row block-footer-bottom">
                    <div class="col-md-12">

                            <span>© 2022 Sunio Team. All rights reserved.</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
