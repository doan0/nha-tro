<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>

<!-- lấy ra thông tin chi tiết nhà trọ -->
  <?php
  $ma_nha_tro = getInput('ma_nha_tro');
  $nhatro = $db->fetchsql("SELECT * FROM nhatro WHERE ma_nha_tro = $ma_nha_tro");
  $machutro = $nhatro['id_chu_tro'];
  $chutro = $db->fetchsql("SELECT * FROM chutro WHERE ma_chu_tro = $machutro");

  ?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>

<!-- code dem so luot xem -->
<?php
$sessionKey1 = 'post_' . $ma_nha_tro;

  if (!isset($_SESSION[$sessionKey1])) { // nếu chưa có session
    $_SESSION[$sessionKey1] = 1;
    dem_lan_xem($ma_nha_tro);
}
function dem_lan_xem($ma_nha_tro)
{
    $db = new Database();
    $sql_count          = "UPDATE nhatro SET luot_xem = luot_xem+1 WHERE ma_nha_tro = $ma_nha_tro";
    $db->query($sql_count);
    return;
}

?>
  <!-- end header nav -->
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <div class="room-list-category">
              <div class="room-detail">
                <div class="title">
                  <a href="javascript: void(0);"><h1><?= $nhatro['tieu_de'] ?></h1></a>
                </div>
                <div class="social">

                  <div class="pull-right">
                    <a href="javascript:void();"><span>Lượt xem : </span><?= $nhatro['luot_xem'] ?></a>
                  </div>
                </div>    <div class="clearfix"></div>
                <!-- Nhà trọ, Phòng trọ -->
                <div class="main-info">
                  <div class="row">
                    <div class="col-md-8 left-info no-padding-right">
                      <div class="address" style="display: flex; flex: shrink 0;">
                        <span class="btn info-label" style="height: 41px; flex-shrink: 0;">Địa chỉ</span>
                        <a class="" href=""><?= $nhatro['dia_chi'] ?></a>
                      </div>
                      <div class="size">
                        <span class="btn info-label">Diện tích</span>
                        <a href="" class="btn info-data"><?= $nhatro['dien_tich'] ?>m<sup>2</sup></a>
                        <span class="btn info-label">Loại phòng</span>
                        <a href="">Phòng trọ, nhà trọ</a>
                      </div>
                      <div class="tang">
                        <span class="btn info-label">Giá phòng</span>
                        <a href="" class="btn info-data"> <?= number_format($nhatro['gia_phong'], 0, ',', '.') ?> Đ/tháng </a>
                      </div>

                      <div class="type">
                        <span class="btn info-label">Giá điện</span>
                        <a href="" class="btn info-data"> <?= number_format($nhatro['gia_dien'], 0, ',', '.') ?> Nghìn/số</a>
                        <span class="btn info-label">Giá nước</span>
                        <a href=""><?= number_format($nhatro['gia_nuoc'], 0, ',', '.') ?> Nghìn/số</a>
                      </div>
                      <div class="type">
                        <span class="btn info-label">Tiện ích</span>
                        <a href=""><?= $nhatro['tien_ich'] ?></a>

                      </div>
                    </div>
                    <div class="col-md-4 info-contact no-padding-left">
                      <div class="contact-label">
                        <a href=""><span class="fa fa-mobile-phone"></span>Thông tin liên hệ</a>
                      </div>
                      <div class="chr"><hr></div>

                      <div class="info-boss">
                        <a href=""><span class="fa fa-user"></span><?= $chutro['ho_ten'] ?></a>
                      </div>
                      <div class="info-phone">
                        <a href=""><span class="fa fa-mobile-phone"></span><?= $chutro['sdt'] ?></a>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="room-detail-des">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="des-label room-list-category">
                        <div class="pull-left">
                          <a href="javascript: void(0);"><h3><span>Thông tin</span> chi tiết <i class="fa fa-angle-right"></i></h3></a>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                      </div>
                      <div class="dis-content"><?= $nhatro['noi_dung'] ?></div>
                      <div id="map" data-lat="<?= $nhatro['vi_do'] ?>" data-lng="<?= $nhatro['kinh_do'] ?>"></div>
                    </div>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- thêm bình luận vào trong csdl -->
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
      $hoten =  $_POST['hoten'];
      $email = $_POST['email'];
      $noidung = $_POST['noidung'];
      $result = $db->query("INSERT INTO binhluan(ho_ten, email, noi_dung, id_nha_tro)
      VALUES('$hoten', '$email', '$noidung', $ma_nha_tro)");

    }
    ?>

    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 room-main-content">
          <h3>Bình luận</h3>
          <div class="col-md-6">
            <form action="" method="POST" class="form-horizontal" role="form">
              <div class="form-group">
                <label for="">Họ tên</label>
                <input type="text" name="hoten" value="" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="">Email</label>
                <input type="email" name="email" value="" class="form-control" required>
              </div>

              <div class="form-group">
                <label for="">Nội dung</label>
                <textarea name="noidung" rows=11 cols=50 class="form-control" required ></textarea>
              </textarea>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary" name="binhluan">Submit</button>
            </div>
          </form>

          <!-- lấy ra danh sách các binh luận của nhà trọ này -->
          <?php
          $danh_sach_bl = $db->query("SELECT * FROM binhluan WHERE id_nha_tro = $ma_nha_tro AND hien_thi = 1");

          ?>
          <?php foreach($danh_sach_bl as $item): ?>
            <div class="media">
              <div class="media-body">
                <?php $timestamp = strtotime($item['thoi_gian']);
                $new_date_format = date('d-m-Y H:i:s', $timestamp); ?>
                <h4 class="media-heading"><?= $item['ho_ten'] ?>
                  <small><?=$new_date_format ?></small>
                </h4>
                <p><?= $item['noi_dung']?></p>
              </div>

            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
<?php  include 'layouts/footer.php';?>
</body>
</html>
