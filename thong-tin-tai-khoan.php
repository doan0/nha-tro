<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>
  <!-- end header nav -->
  <?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();
    $datas = [
      "ho_ten" => postInput("ho_ten"),
      "sdt" => postInput("sdt"),
      "ten_dang_nhap" => postInput("ten_dang_nhap"),
      "mat_khau" => md5(postInput("mat_khau"))
    ];

    if (postInput("ho_ten") == NULL) {
      $error['ho_ten'] = 'Họ tên không được trống';
    }

    if (postInput("sdt") == NULL) {
      $error['sdt'] = 'Số điện thoại không được trống';
    }

    if (postInput("ten_dang_nhap") == NULL) {
      $error['ten_dang_nhap'] = 'Tên đăng nhập không được trống';
    }

    if (postInput("mat_khau") == NULL) {
      $error['mat_khau'] = 'mật khẩu không được trống';
    }

    if (postInput("repassword") == NULL) {
      $error['repassword'] = 'Vui lòng nhập lại mật khẩu';
    }

    if (postInput("repassword") != postInput("mat_khau")) {
      $error['repassword1'] = 'Mật khẩu không khớp';
    }

    if (empty($error)) {
      $isset = $db->fetchOne("chutro", "ten_dang_nhap		 = '" . $datas['ten_dang_nhap'] . "'");
      if ($isset > 0) {
        $_SESSION['error'] = "Tên đăng nhập đã tồn tại";
      } else {

         $result = $db->insert('chutro', $datas);
        if (isset($result)) {
          echo "<script>alert('Bạn đã đăng kí thành công');location.href='dang-nhap.php'</script>";
        }
      }
    }

  }
  ?>
  <!-- lấy thông tin tài khoản -->
  <?php
  $ma_chu_tro = $_SESSION['chutro_id'];
  $taikhoan = $db->fetchsql("select * from chutro where ma_chu_tro = $ma_chu_tro");
  ?>

  <!-- neu người dùng ấn nút cập nhật thì cập nhật thông tin -->
  <?php if (isset($_POST['capnhat'])){
    $ho_ten = $_POST['ho_ten'];
    $sdt = $_POST['sdt'];
    $result = $db->query("UPDATE chutro SET ho_ten = '$ho_ten', sdt = '$sdt' WHERE ma_chu_tro = $ma_chu_tro");
    if ($result) {
        echo "<script>alert('Cập nhật tài khoản thành công');location.href='index.php'</script>";
    }
  }
  ?>

  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <h2 class="text text-success">Thông tin cá nhân</h2>

            <div class="col-md-8 col-md-offset-2 login-page">
              <form class="form-horizontal" method="POST" action="">
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Họ và tên</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="ho_ten" class="form-control"   value="<?= $taikhoan['ho_ten'] ?>">

                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tên đăng nhập</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="ten_dang_nhap" class="form-control" value="<?= $taikhoan['ten_dang_nhap'] ?>" disabled>

                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_email" class="col-sm-4 control-label">Số điện thoại</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="sdt" class="form-control" id="email" value="<?= $taikhoan['sdt'] ?>">

                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary" name="capnhat">Cập nhật</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php  include 'layouts/footer.php';?>

</body>
</html>
