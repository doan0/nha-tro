
<?php include "autoload/autoload.php" ?>

<?php
if (!isset($_SESSION['ten_chutro'])) {
  echo '<script type="text/javascript">alert("Bạn phải đăng nhập !");
  window.location.href = "<?= base_url() ?>dang-nhap.php";
  </script>';

}
?>

<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>

  <?php
    $provinces = $db->fetchAll('province');
  // lay thông tin của bài viết
  $ma_nha_tro = $_GET['ma_nha_tro'];
  $data = $db->fetchsql("select * from nhatro where ma_nha_tro = $ma_nha_tro");

  // neu chon update thì tiến hành update dữ liệu
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();
    if (postInput("tieude") == NULL) {
      $error['tieude'] = 'Tiêu đề không được trống';
    } else{
      $tieude= postInput("tieude");
    }

    if (postInput("noidung") == NULL) {
      $error['noidung'] = 'Nội dung không được trống';
    } else{
      $noidung= postInput("noidung");
    }

    if (postInput("giaphong") == NULL) {
      $error['giaphong'] = 'Gía phòng không được trống';
    } else{
      $giaphong= postInput("giaphong");
    }

    if (postInput("giadien") == NULL) {
      $error['giadien'] = 'Gía điện không được trống';
    } else{
      $giadien= postInput("giadien");
    }

    if (postInput("gianuoc") == NULL) {
      $error['gianuoc'] = 'Gía nước không được trống';
    } else{
      $gianuoc= postInput("gianuoc");
    }

    if (postInput("dientich") == NULL) {
      $error['dientich'] = 'Diện tích không được trống';
    } else{
      $dientich= postInput("dientich");
    }

    if (postInput("diachi") == NULL) {
      $error['diachi'] = 'Địa chỉ không được trống';
    } else{
      $diachi= postInput("diachi");
    }

    if (postInput("tienich") == NULL) {
      $tienich = 'Chưa xác định';
    } else{
      $tienich= postInput("tienich");
    }

    $id_chu_tro = $_SESSION['chutro_id'];

    if (empty($error)) {

      if ($_FILES['hinhanh']['size'] == '') {
        $file_name = $data['hinh_anh'];
      }
      else{

        $file_name = time().$_FILES['hinhanh']['name'];
        unlink('public/uploads/phongtro/'.$data['hinh_anh']);
        move_uploaded_file($_FILES['hinhanh']['tmp_name'], 'public/uploads/phongtro/' . $file_name);

      }

      $result = $db->query("UPDATE nhatro SET tieu_de = '$tieude', noi_dung = '$noidung', gia_phong = $giaphong, gia_dien = $giadien, gia_nuoc = $gianuoc, dien_tich = $dientich, tien_ich = '$tienich', dia_chi = '$diachi', hinh_anh = '$file_name' WHERE ma_nha_tro = $ma_nha_tro");

      if ($result) {
        echo '<script type="text/javascript">alert("Cập nhật thành công !");
        window.location.href = "<?= base_url() ?>";
        </script>';
      }
    }

  }
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <h2 class="text text-success">Thôn tin bài viết</h2>

            <div class="col-md-12 login-page">
              <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tiêu đề</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="tieude" class="form-control" id="username" value="<?= $data['tieu_de'] ?>" >
                    <?php
                    if (isset($error['tieude'])) echo "<span class='help-block'><span style='color:red;'>" . $error['tieude']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Nội dung</label>
                  <div class="col-sm-8 col-md-8">
                    <textarea name="noidung" rows="8" class="form-control">
                      <?php echo $data['noi_dung'] ?>
                    </textarea>
                    <?php
                    if (isset($error['noidung'])) echo "<span class='help-block'><span style='color:red;'>" . $error['noidung']. "</span></span>";
                    ?>
                    <script>

                    CKEDITOR.replace('noidung');

                    </script>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía phòng</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="giaphong" class="form-control" value="<?= $data['gia_phong'] ?>">
                    <?php
                    if (isset($error['giaphong'])) echo "<span class='help-block'><span style='color:red;'>" . $error['giaphong']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía điện</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="giadien" class="form-control"  placeholder="Gía điện ..." value="<?= $data['gia_dien']?>">
                    <?php
                    if (isset($error['giadien'])) echo "<span class='help-block'><span style='color:red;'>" . $error['giadien']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía nước</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="gianuoc" class="form-control"  placeholder="Gía nước ..." value="<?= $data['gia_nuoc'] ?>">
                    <?php
                    if (isset($error['gianuoc'])) echo "<span class='help-block'><span style='color:red;'>" . $error['gianuoc']. "</span></span>";
                    ?>
                  </div>
                </div>


                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Diện tích</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="dientich" class="form-control"  placeholder="Diện tích ..." value="<?= $data['dien_tich'] ?>">
                    <?php
                    if (isset($error['dientich'])) echo "<span class='help-block'><span style='color:red;'>" . $error['dientich']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tiện ích</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="tienich" class="form-control"  placeholder="Tiện ích ..." value="<?= $data['tien_ich'] ?>">

                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Địa chỉ</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="diachi" class="form-control"  placeholder="Địa chỉ ..." value="<?= $data['dia_chi'] ?>">
                    <?php
                    if (isset($error['diachi'])) echo "<span class='help-block'><span style='color:red;'>" . $error['diachi']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tỉnh/Thành phố</label>
                  <div class="col-sm-8 col-md-8">
                    <select name="province_id" class="form-control" id="province">
                      <?php foreach($provinces as $item) :?>
                        <option class="province" value="<?= $item['id'] ?>"><?= $item['_name'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <?php
                    if (isset($error['province_id'])) echo "<span class='help-block'><span style='color:red;'>" . $error['province_id']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Quận/Huyện</label>
                  <div class="col-sm-8 col-md-8">
                    <select name="district_id" class="form-control" id="district">
                    </select>
                    <?php
                    if (isset($error['district_id'])) echo "<span class='help-block'><span style='color:red;'>" . $error['district_id']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Hình ảnh</label><br>
                  <div class="col-sm-8 col-md-8">
                    <img src="<?= base_url() ?>public/uploads/phongtro/<?= $data['hinh_anh'] ?>" alt="" width="200px" height="150px"><br><br>

                    <input type="file" name="hinhanh">

                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary">update</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php  include 'layouts/footer.php';?>
  <script>
    getDistrict(1);
    $('#province').on('change', function() {
      const id = $(this).val();
      getDistrict(id);
    });
    function getDistrict(province_id) {
        const baseUrl = '<?= base_url() ?>';
        $.ajax({
          url:  baseUrl + 'api/district.php',
          type: 'get',
          data: {
            id:province_id,
          },
          dataType: 'json',
          success: function(result) {
            result
            var html = '';
            $.each(result, function(key, item) {
              var value = item['id'];
              var name = item['_name'];
              html += '<option value="' + value + '">' + name + '</option>';
            });
            $('#district').html(html);
          }
        });
    }
  </script>
</body>
</html>
