<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>
  <!-- end header nav -->
  <?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();
    $datas = [
      "ho_ten" => postInput("ho_ten"),
      "sdt" => postInput("sdt"),
      "ten_dang_nhap" => postInput("ten_dang_nhap"),
      "mat_khau" => md5(postInput("mat_khau"))
    ];

    if (postInput("ho_ten") == NULL) {
      $error['ho_ten'] = 'Họ tên không được trống';
    }

    if (postInput("sdt") == NULL) {
      $error['sdt'] = 'Số điện thoại không được trống';
    }

    if (postInput("ten_dang_nhap") == NULL) {
      $error['ten_dang_nhap'] = 'Tên đăng nhập không được trống';
    }

    if (postInput("mat_khau") == NULL) {
      $error['mat_khau'] = 'mật khẩu không được trống';
    }

    if (postInput("repassword") == NULL) {
      $error['repassword'] = 'Vui lòng nhập lại mật khẩu';
    }

    if (postInput("repassword") != postInput("mat_khau")) {
      $error['repassword1'] = 'Mật khẩu không khớp';
    }

    if (empty($error)) {
      $isset = $db->fetchOne("chutro", "ten_dang_nhap		 = '" . $datas['ten_dang_nhap'] . "'");
      if ($isset > 0) {
        $_SESSION['error'] = "Tên đăng nhập đã tồn tại";
      } else {

         $result = $db->insert('chutro', $datas);
        if (isset($result)) {
          echo "<script>alert('Bạn đã đăng kí thành công');location.href='dang-nhap.php'</script>";
        }
      }
    }

  }
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <h2 class="text text-success">Đăng ký tài khoản</h2>

            <div class="col-md-8 col-md-offset-2 login-page">
              <form class="form-horizontal" method="POST" action="">
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Họ và tên</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="ho_ten" class="form-control"  placeholder="Họ và tên ..." value="<?= old("ho_ten") ?>">
                    <?php
                    if (isset($error['ho_ten'])) echo "<span class='help-block'><span style='color:red;'>" . $error['ho_ten']. "</span></span>";
                    ?>

                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tên đăng nhập</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="ten_dang_nhap" class="form-control" id="username" placeholder="Tên hiển thị ..." value="<?= old("ten_dang_nhap") ?>">
                    <?php
                    if (isset($error['ten_dang_nhap'])) echo "<span class='help-block'><span style='color:red;'>" . $error['ten_dang_nhap']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_email" class="col-sm-4 control-label">Số điện thoại</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="sdt" class="form-control" id="email" placeholder="Số điện thoại ..." value="<?= old("sdt") ?>">
                    <?php
                    if (isset($error['sdt'])) echo "<span class='help-block'><span style='color:red;'>" . $error['sdt']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_password" class="col-sm-4 control-label">Mật khẩu</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="password" name="mat_khau" class="form-control" id="password" placeholder="Mật khẩu ..." value="<?= old("mat_khau") ?>">
                    <?php
                    if (isset($error['mat_khau'])) echo "<span class='help-block'><span style='color:red;'>" . $error['mat_khau']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_password2" class="col-sm-4 control-label">Xác nhận mật khẩu</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="password" name="repassword" class="form-control" id="password_confirmation" placeholder="Xác nhận mật khẩu ..." value="<?= old("repassword") ?>">
                    <?php
                    if (isset($error['repassword'])) echo "<span class='help-block'><span style='color:red;'>" . $error['repassword']. "</span></span>";
                    else if(isset($error['repassword1'])) echo "<span class='help-block'><span style='color:red;'>" . $error['repassword1']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-auth-submit">Đăng ký tài khoản</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php  include 'layouts/footer.php';?>

</body>
</html>
