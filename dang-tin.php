
<?php include "autoload/autoload.php" ?>

<?php
if (!isset($_SESSION['ten_chutro'])) {
  echo '<script type="text/javascript">alert("Bạn phải đăng nhập !");
  window.location.href = "<?= base_url() ?>/dang-nhap.php";
  </script>';

}
?>

<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>

  <?php
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $error = array();
    if (postInput("tieude") == NULL) {
      $error['tieude'] = 'Tiêu đề không được trống';
    } else{
      $tieude= postInput("tieude");
    }

    if (postInput("noidung") == NULL) {
      $error['noidung'] = 'Nội dung không được trống';
    } else{
      $noidung= postInput("noidung");
    }

    if (postInput("giaphong") == NULL) {
      $error['giaphong'] = 'Gía phòng không được trống';
    } else{
      $giaphong= postInput("giaphong");
    }

    if (postInput("giadien") == NULL) {
      $error['giadien'] = 'Gía điện không được trống';
    } else{
      $giadien= postInput("giadien");
    }

    if (postInput("gianuoc") == NULL) {
      $error['gianuoc'] = 'Gía nước không được trống';
    } else{
      $gianuoc= postInput("gianuoc");
    }

    if (postInput("dientich") == NULL) {
      $error['dientich'] = 'Diện tích không được trống';
    } else{
      $dientich= postInput("dientich");
    }

    if (postInput("diachi") == NULL) {
      $error['diachi'] = 'Địa chỉ không được trống';
    } else{
      $diachi= postInput("diachi");
    }

    if (postInput("tienich") == NULL) {
      $tienich = 'Chưa xác định';
    } else{
      $tienich= postInput("tienich");
    }

    if ($_FILES['hinhanh']['name'] == null) {
      $error['hinhanh'] = "Bạn chưa chọn hình ảnh";
    }
    $image = $_FILES['hinhanh']['name'];
    $image_name = time() . '.' . $image;
    $id_chu_tro = $_SESSION['chutro_id'];
    $danhmuc= postInput("danhmuc");
    $province_id= postInput("province_id");
    $district_id= postInput("district_id");
    $kinh_do= postInput("kinh_do");
    $vi_do= postInput("vi_do");

    if (empty($error)) {
      move_uploaded_file($_FILES['hinhanh']['tmp_name'], 'public/uploads/phongtro/' . $image_name);
      $result = $db->query("INSERT INTO nhatro(tieu_de, noi_dung, gia_phong, gia_dien, gia_nuoc, dien_tich, tien_ich, dia_chi, 	hinh_anh, id_chu_tro, id_danh_muc, province_id, district_id, kinh_do, vi_do)
            VALUES('$tieude' , '$noidung', $giaphong, $giadien, $gianuoc, $dientich, '$tienich', '$diachi', '$image_name', $id_chu_tro, $danhmuc, $province_id, $district_id, $kinh_do, $vi_do)");

      $baseUrl = base_url();
      if ($result) {
        echo "<script type='text/javascript'>alert('Đăng tin thành công !');
        window.location.href = '$baseUrl';
        </script>";
      }
    }

  }
  $provinces = $db->fetchAll('province');
  ?>
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <h2 class="text text-success">Nhập thông tin cần đăng</h2>

            <div class="col-md-12 login-page">
              <form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tiêu đề</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="tieude" class="form-control" id="username" placeholder="Tiêu đề ..." value="<?= old("tieude") ?>" >
                    <?php
                    if (isset($error['tieude'])) echo "<span class='help-block'><span style='color:red;'>" . $error['tieude']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Danh mục</label>
                  <div class="col-sm-8 col-md-8">
                    <select name="danhmuc" class="form-control">
                      <?php foreach($categories as $item) :?>
                        <option value="<?= $item['id'] ?>"><?= $item['ten_danh_muc'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <?php
                    if (isset($error['tieude'])) echo "<span class='help-block'><span style='color:red;'>" . $error['danhmuc']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Nội dung</label>
                  <div class="col-sm-8 col-md-8">
                    <textarea name="noidung" rows="8" class="form-control">
                      <?php echo old("noidung") ?>
                    </textarea>
                    <?php
                    if (isset($error['noidung'])) echo "<span class='help-block'><span style='color:red;'>" . $error['noidung']. "</span></span>";
                    ?>
                    <script>

                    CKEDITOR.replace('noidung');

                    </script>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía phòng</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="giaphong" class="form-control"  placeholder="Gía phòng ..." value="<?= old("giaphong") ?>">
                    <?php
                    if (isset($error['giaphong'])) echo "<span class='help-block'><span style='color:red;'>" . $error['giaphong']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía điện</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="giadien" class="form-control"  placeholder="Gía điện ..." value="<?= old("giadien") ?>">
                    <?php
                    if (isset($error['giadien'])) echo "<span class='help-block'><span style='color:red;'>" . $error['giadien']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Gía nước</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="gianuoc" class="form-control"  placeholder="Gía nước ..." value="<?= old("gianuoc") ?>">
                    <?php
                    if (isset($error['gianuoc'])) echo "<span class='help-block'><span style='color:red;'>" . $error['gianuoc']. "</span></span>";
                    ?>
                  </div>
                </div>


                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Diện tích</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="dientich" class="form-control"  placeholder="Diện tích ..." value="<?= old("dientich") ?>">
                    <?php
                    if (isset($error['dientich'])) echo "<span class='help-block'><span style='color:red;'>" . $error['dientich']. "</span></span>";
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tiện ích</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="tienich" class="form-control"  placeholder="Tiện ích ..." value="<?= old("tienich") ?>">

                  </div>
                </div>

                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Địa chỉ</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="diachi" class="form-control"  placeholder="Địa chỉ ..." value="<?= old("diachi") ?>">
                    <?php
                    if (isset($error['diachi'])) echo "<span class='help-block'><span style='color:red;'>" . $error['diachi']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Tỉnh/Thành phố</label>
                  <div class="col-sm-8 col-md-8">
                    <select name="province_id" class="form-control" id="province">
                      <?php foreach($provinces as $item) :?>
                        <option class="province" value="<?= $item['id'] ?>"><?= $item['_name'] ?></option>
                      <?php endforeach; ?>
                    </select>
                    <?php
                    if (isset($error['province_id'])) echo "<span class='help-block'><span style='color:red;'>" . $error['province_id']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Quận/Huyện</label>
                  <div class="col-sm-8 col-md-8">
                    <select name="district_id" class="form-control" id="district">
                    </select>
                    <?php
                    if (isset($error['district_id'])) echo "<span class='help-block'><span style='color:red;'>" . $error['district_id']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Vĩ độ</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="vi_do" class="form-control"  placeholder="Vĩ độ..." value="<?= old("vi_do") ?>">
                    <?php
                    if (isset($error['vi_do'])) echo "<span class='help-block'><span style='color:red;'>" . $error['vi_do']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Kinh độ</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="kinh_do" class="form-control"  placeholder="Kinh độ..." value="<?= old("kinh_do") ?>">
                    <?php
                    if (isset($error['kinh_do'])) echo "<span class='help-block'><span style='color:red;'>" . $error['kinh_do']. "</span></span>";
                    ?>
                  </div>
                </div>
                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Hình ảnh</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="file" name="hinhanh">
                    <?php
                    if (isset($error['hinhanh'])) echo "<span class='help-block'><span style='color:red;'>" . $error['hinhanh']. "</span></span>";
                    ?>
                  </div>
                </div>



                <h2 class="text text-success">Thông tin liên hệ</h2>


                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Người Liên hệ</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="nguoilienhe" class="form-control" value="<?= $_SESSION['ten_chutro']?>">

                  </div>
                </div>


                <div class="form-group">
                  <label for="txt_fullname" class="col-sm-4 control-label">Số điện thoại</label>
                  <div class="col-sm-8 col-md-8">
                    <input type="text" name="sdt" class="form-control" value="<?= $_SESSION['chutro_sdt']?>">

                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-primary">Đăng tin</button>
                  </div>
                </div>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php  include 'layouts/footer.php';?>
  <script>
    getDistrict(1);
    $('#province').on('change', function() {
      const id = $(this).val();
      getDistrict(id);
    });
    function getDistrict(province_id) {
        const baseUrl = '<?= base_url() ?>';
        $.ajax({
          url:  baseUrl + 'api/district.php',
          type: 'get',
          data: {
            id:province_id,
          },
          dataType: 'json',
          success: function(result) {
            result
            var html = '';
            $.each(result, function(key, item) {
              var value = item['id'];
              var name = item['_name'];
              html += '<option value="' + value + '">' + name + '</option>';
            });
            $('#district').html(html);
          }
        });
    }
  </script>
</body>
</html>
