
<?php
include '../../autoload/autoload.php';

include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    include '../../layouts/header.php';
    ?>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <?php
    include '../../layouts/sidebar.php';
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <section class="content">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Danh sách bài đăng</h3>
          </div>
          <div class="box-body">
            <?php require_once '../../../partials/notification.php'; ?>
            <!-- lay danh sách chủ trọ va phan trang-->
            <?php
            if (isset($_GET['p'])) {
              $p = $_GET['p'];
            } else {
              $p = 1;
            }
            $total = $db->countTable('ma_nha_tro', 'nhatro');
            $sql = "SELECT nhatro.ma_nha_tro, nhatro.tieu_de, nhatro.gia_phong, nhatro.gia_dien, nhatro.gia_nuoc, nhatro.dien_tich, nhatro.id_chu_tro, nhatro.hien_thi, chutro.ho_ten, chutro.ma_chu_tro, chutro.ten_dang_nhap
            FROM nhatro INNER JOIN chutro ON nhatro.id_chu_tro  =  chutro.ma_chu_tro ORDER BY ma_nha_tro DESC ";
            $data = $db->fetchJones("nhatro", $sql, $total, $p, 7, true);
            $total_page = $data['page'];
            unset($data['page']);
            ?>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>STT</th>
                    <th>Tiêu đề</th>
                    <th>Diện tích</th>
                    <th>Giá phòng</th>
                    <th>Hiện thị</th>
                    <th>Tài khoản đăng</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $stt = 0; ?>
                  <?php foreach ($data as $item): $stt++?>

                    <tr>
                      <td><?= $stt; ?></td>
                      <td><?= $item['tieu_de']; ?></td>
                      <td><?= $item['dien_tich']; ?> m<sup>2</sup></td>
                      <td><?= number_format($item['gia_phong'], 0, ',', '.') ?></td>
                      <td>
                        <a href="hien-thi.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>"
                          class="<?php echo $item['hien_thi'] == 0 ? "btn-xs btn-danger" : "btn-xs btn-info"; ?>">
                          <?php echo $item['hien_thi'] == 1 ? "Hiển thị" : "Không hiển thị"; ?>
                        </a>
                      </td>
                      <td><?= $item['ten_dang_nhap']; ?></td>

                      <td>
                        <a onclick="return confirm('Bạn chắc chắn xoa ?')" href="delete.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
              <div style="text-align: center">
                <nav aria-label="...">
                  <ul class="pagination">

                    <?php for ($i = 1; $i <= $total_page; $i++): ?>
                      <?php
                      if (isset($_GET['p']) && $_GET['p'] == $i) {
                        $active = "active";
                      } else {
                        $active = "";
                      }

                      ?>
                      <li class="page-item <?php echo $active ?>">
                        <a class="page-link" href="?p=<?= $i ?>"><?= $i ?></a>
                      </li>
                    <?php endfor ?>

                  </ul>
                </nav>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    include '../../layouts/footer.php';
    if (!isset($_GET['p'])) {
      echo '<script>
      $(".pagination li:nth-child(1)").addClass("active");
      </script>';
    } ?>


  </div>

  <!-- ./wrapper -->
