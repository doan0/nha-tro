
<?php
include '../../autoload/autoload.php';

include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php
  include '../../layouts/header.php';
  ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php
  include '../../layouts/sidebar.php';
  ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Danh sách bình luận</h3>
        </div>
        <div class="box-body">
          <?php require_once '../../../partials/notification.php'; ?>
          <!-- lay danh sách binh luan va phan trang-->
          <?php
          if (isset($_GET['p'])) {
            $p = $_GET['p'];
          } else {
            $p = 1;
          }
          $total = $db->countTable('ma_binh_luan', 'binhluan');
          $sql = "SELECT binhluan.ma_binh_luan, binhluan.noi_dung, binhluan.id_nha_tro, binhluan.hien_thi, nhatro.ma_nha_tro, nhatro.tieu_de
          FROM binhluan INNER JOIN nhatro ON binhluan.id_nha_tro = nhatro.ma_nha_tro ORDER BY binhluan.ma_binh_luan DESC";
          $data = $db->fetchJones("binhluan", $sql, $total, $p, 5, true);
          $total_page = $data['page'];
          unset($data['page']);
           ?>
          <div class="table-responsive">
                 <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Bài viết</th>
                      <th>Nội dung</th>
                      <th>Hiển thị</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $stt = 0; ?>
                    <?php foreach ($data as $item): $stt++?>

                    <tr>
                      <td><?= $stt; ?></td>
                      <td><?= $item['tieu_de']; ?></td>
                      <td><?= $item['noi_dung']; ?></td>
                      <td>
                        <a href="hien-thi.php?ma_binh_luan=<?= $item['ma_binh_luan'] ?>"
                          class="<?php echo $item['hien_thi'] == 0 ? "btn-xs btn-danger" : "btn-xs btn-info"; ?>">
                          <?php echo $item['hien_thi'] == 1 ? "Hiển thị" : "Không hiển thị"; ?>
                        </a>
                      </td>
                      <td>
                        <a onclick="return confirm('Bạn chắc chắn xoa ?')" href="delete.php?ma_binh_luan=<?= $item['ma_binh_luan'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                 </table>
                 <div style="text-align: center">
                   <nav aria-label="...">
                     <ul class="pagination">

                       <?php for ($i = 1; $i <= $total_page; $i++): ?>
                         <?php
                         if (isset($_GET['p']) && $_GET['p'] == $i) {
                           $active = "active";
                         } else {
                           $active = "";
                         }

                         ?>
                         <li class="page-item <?php echo $active ?>">
                           <a class="page-link" href="?p=<?= $i ?>"><?= $i ?></a>
                         </li>
                       <?php endfor ?>

                     </ul>
                   </nav>
                 </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
  include '../../layouts/footer.php';
  if (!isset($_GET['p'])) {
    echo '<script>
    $(".pagination li:nth-child(1)").addClass("active");
    </script>';
  } ?>


</div>
<!-- ./wrapper -->
