
<?php
include '../../autoload/autoload.php';

include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php
  include '../../layouts/header.php';
  ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php
  include '../../layouts/sidebar.php';
  ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Danh sách chủ trọ</h3>
        </div>
        <div class="box-body">
          <?php require_once '../../../partials/notification.php'; ?>
          <!-- lay danh sách chủ trọ va phan trang -->
          <?php
          if (isset($_GET['p'])) {
            $p = $_GET['p'];
          } else {
            $p = 1;
          }
          $total = $db->countTable('ma_chu_tro', 'chutro');

          $sql = "SELECT * FROM chutro ORDER BY ma_chu_tro DESC";
          $data = $db->fetchJones("chutro", $sql, $total, $p, 5, true);
          $total_page = $data['page'];
          unset($data['page']);
          ?>
          <div class="table-responsive">
                 <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Họ tên</th>
                      <th>Tên đăng nhập</th>
                      <th>Số điện thoại</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $stt = 0; ?>
                    <?php foreach ($data as $item): $stt++?>

                    <tr>
                      <td><?= $stt; ?></td>
                      <td><?= $item['ho_ten']; ?></td>
                      <td><?= $item['ten_dang_nhap']; ?></td>
                      <td><?= $item['sdt']; ?></td>
                      <td>
                        <a onclick="return confirm('Bạn chắc chắn xoa ?')" href="delete.php?ma_chu_tro=<?= $item['ma_chu_tro'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                 </table>
                 <div style="text-align: center">
                   <nav aria-label="...">
                     <ul class="pagination">

                       <?php for ($i = 1; $i <= $total_page; $i++): ?>
                         <?php
                         if (isset($_GET['p']) && $_GET['p'] == $i) {
                           $active = "active";
                         } else {
                           $active = "";
                         }

                         ?>
                         <li class="page-item <?php echo $active ?>">
                           <a class="page-link" href="?p=<?= $i ?>"><?= $i ?></a>
                         </li>
                       <?php endfor ?>

                     </ul>
                   </nav>
                 </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
  include '../../layouts/footer.php';
  if (!isset($_GET['p'])) {
    echo '<script>
    $(".pagination li:nth-child(1)").addClass("active");
    </script>';
  } ?>

</div>
<!-- ./wrapper -->
