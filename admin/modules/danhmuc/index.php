
<?php
    $open = "danhmuc";
    include '../../autoload/autoload.php';
 ?>

<?php

include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php
  include '../../layouts/header.php';
  ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php
  include '../../layouts/sidebar.php';
  ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
<?php $danhmuc = $db->fetchAll('danhmuc'); ?>
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <?php require_once '../../../partials/notification.php'; ?>

        <div class="box-header with-border">
          <h3 class="box-title">danh sách danh mục</h3>
        </div>

        <div class="box-body">
          <div class="form-group">
            <a href="add.php" class="btn btn-primary">Create</a>
          </div>
          <div class="table-responsive">
                 <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tên danh mục</th>
                      <th>Thao tác</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $stt = 0; ?>
                    <?php foreach ($danhmuc as $item): $stt++?>

                    <tr>
                      <td><?= $stt; ?></td>
                      <td><?= $item['ten_danh_muc']; ?></td>
                      <td>
                        <a href="edit.php?id=<?= $item['ma_danh_muc'] ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>
                        <a onclick="return confirm('Bạn chắc chắn xoa ?')" href="delete.php?id=<?= $item['ma_danh_muc'] ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                 </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
  include '../../layouts/footer.php';
  ?>
</div>
<!-- ./wrapper -->
