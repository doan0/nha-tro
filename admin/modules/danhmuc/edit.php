
<?php
$open = "danhmuc";
include '../../autoload/autoload.php';

?>

<?php
include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    include '../../layouts/header.php';
    ?>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <?php
    include '../../layouts/sidebar.php';
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <?php
      $id = getInput('id');
      $data = $db->fetchsql("select * from danhmuc where ma_danh_muc = $id");
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (postInput("name") == NULL) {
          $error['name'] = 'Tên danh mục không được trống';
        } else {
          $name = $_POST['name'];
        }


        if (empty($error)) {

          $isset = $db->fetchOne("danhmuc", "ten_danh_muc	 = '" . $name . "'");
          if ($isset > 0) {
            $_SESSION['success'] = "Dữ liệu không thay đổi";
            redirectAdmin("danhmuc");
          } else {
            $result = $db->query("UPDATE danhmuc SET ten_danh_muc = '$name' WHERE ma_danh_muc = $id");
            if (isset($result)) {
              $_SESSION['success'] = "Sửa  thành công";
              redirectAdmin("danhmuc");
            } else {
              $_SESSION['success'] = "Thêm mới thất bại";
            }
          }


        }
      }
      ?>

      <section class="content">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Sửa danh mục</h3>
          </div>
          <div class="box-body">
            <?php require_once '../../../partials/notification.php'; ?>

            <form action="" method="POST" role="form">

              <div class="form-group">
                <label for="">Tên danh mục</label>
                <input type="text" class="form-control" name="name"  value="<?php echo $data['ten_danh_muc'];?>">
                <?php
                if (isset($error['name'])) echo "<p class='text-danger'>" . $error['name'];
                ?>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    include '../../layouts/footer.php';
    ?>
  </div>
  <!-- ./wrapper -->
