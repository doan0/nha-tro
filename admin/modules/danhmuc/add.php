
<?php
$open = "danhmuc";
include '../../autoload/autoload.php';

?>

<?php
include '../../layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    include '../../layouts/header.php';
    ?>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <?php
    include '../../layouts/sidebar.php';
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <?php
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $datas = [
          "ten_danh_muc" => postInput("name"),

        ];

        $error = [];

        if (postInput("name") == NULL) {
          $error['name'] = 'Tên danh mục không được trống';
        }


        if (empty($error)) {

          $isset = $db->fetchOne("danhmuc", "ten_danh_muc	 = '" . $datas['ten_danh_muc'] . "'");
          if ($isset > 0) {
            $_SESSION['error'] = "Tên đã tồn tại";
          } else {
            $result = $db->insert('danhmuc', $datas);
            if (isset($result)) {
              $_SESSION['success'] = "Thêm mới thành công";
              redirectAdmin("danhmuc");
            } else {
              $_SESSION['success'] = "Thêm mới thất bại";
            }
          }


        }
      }
      ?>
      <section class="content">
        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Thêm danh mục</h3>
          </div>
          <div class="box-body">
            <?php require_once '../../../partials/notification.php'; ?>

            <form action="" method="POST" role="form">

              <div class="form-group">
                <label for="">Tên danh mục</label>
                <input type="text" class="form-control" name="name" placeholder="Tên danh mục" value="<?php echo old('name')?>">
                <?php
                if (isset($error['name'])) echo "<p class='text-danger'>" . $error['name'];
                ?>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    include '../../layouts/footer.php';
    ?>
  </div>
  <!-- ./wrapper -->
