
<?php
$open = "dashboard";
include 'autoload/autoload.php';
if (!isset($_SESSION['amin_hoten'])) {
  header('Location: login.php');
}
include 'layouts/heade.php';
?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <?php
  include 'layouts/header.php';
  ?>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <?php
  include 'layouts/sidebar.php';
  ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Chào mừng bạn đến với trang quản trị</h3>
        </div>
        <div class="box-body">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-th"></i></span>

            <div class="info-box-content">
              <a href="<?php echo modules('baidang') ?>">
                <span class="info-box-text">Bài đăng</span>
                <?php $total_cate = $db->countTable('ma_nha_tro', 'nhatro'); ?>
                <span class="info-box-number"><?= $total_cate ?></span>
              </a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                  <a href="<?php echo modules('chutro') ?>">
                    <span class="info-box-text">Chủ trọ</span>
                    <?php $chutro= $db->countTable('ma_chu_tro', 'chutro'); ?>
                    <span class="info-box-phone"><?= $chutro ?></span>
                  </a>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-orange"><i class="fa fa-comment"></i></span>

                    <div class="info-box-content">
                      <a href="<?php echo modules('binhluan') ?>">
                        <span class="info-box-text">Bình luận</span>
                        <?php $binhluan= $db->countTable('ma_binh_luan', 'binhluan'); ?>
                        <span class="info-box-phone"><?= $binhluan ?></span>
                      </a>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
        </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
  include 'layouts/footer.php';
  ?>
</div>
<!-- ./wrapper -->
