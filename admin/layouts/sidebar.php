<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?php echo(isset($open) && $open == 'dashboard' ? 'active' : '') ?>">
         <a href="<?= base_url() ?>admin">
           <i class="fa fa-dashboard"></i> <span>Dashboard</span>

         </a>

       </li>

      <li>
        <?php $chutro = $db->countTable('ma_chu_tro', 'chutro'); ?>
        <a href="<?= modules("chutro") ?>">
          <i class="fa fa-th"></i> <span>Quản lý chủ trọ</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green"><?= $chutro ?></small>
          </span>
        </a>
      </li>

      <li>
        <?php $nhatro = $db->countTable('ma_nha_tro', 'nhatro'); ?>
        <a href="<?= modules("baidang") ?>">
          <i class="fa fa-th"></i> <span>Quản lý bài đăng</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green"><?= $nhatro ?></small>
          </span>
        </a>
      </li>

      <li>
        <?php $nhatro = $db->countTable('ma_binh_luan', 'binhluan'); ?>
        <a href="<?= modules("binhluan") ?>">
          <i class="fa fa-th"></i> <span>Quản lý bình luận</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green"><?= $nhatro ?></small>
          </span>
        </a>
      </li>
      <li>
        <?php $nhatro = $db->countTable('ma_binh_luan', 'binhluan'); ?>
        <a href="<?= modules("danhmuc") ?>">
          <i class="fa fa-th"></i> <span>Quản lý loại phòng</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green"><?= $nhatro ?></small>
          </span>
        </a>
      </li>

      <li>
        <a href="#">
         <i class="fa fa-calendar"></i> <span>Lịch</span>
         <span class="pull-right-container">
           <small class="label pull-right bg-yellow"><?php echo date("Y") ?></small>

           <small class="label pull-right bg-red"><?php echo date("m") ?></small>
           <small class="label pull-right bg-blue"><?php echo date("d") ?></small>
         </span>
       </a>
     </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
