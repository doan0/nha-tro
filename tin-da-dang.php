<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>

<!-- lay ra danh sach bai viet của tài khoản đang đăng nhập -->
  <?php
  $machutro = $_SESSION['chutro_id'];

  $phongtro = $db->query("SELECT * FROM nhatro WHERE id_chu_tro = $machutro ORDER BY ma_nha_tro DESC");
  ?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>
  <!-- end header nav -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <div class="room-list-category">
              <div class="menu-tab">
                <div class="menu-tab-title"><a href=""><span>Danh sách tin đã đăng</span> <i class="fa fa-angle-right"></i></a></div>
              </div>
              <br><br><br>
              <!-- end menu-tab -->
              <div class="tab-content">

                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Tiêu đề</th>
                      <th>Trạng thái</th>
                      <th>Hành động</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php $stt = 0; ?>
                      <?php foreach($phongtro as $item) : $stt ++?>
                    <tr>
                      <td><?=  $stt;?></td>
                      <td><?= $item['tieu_de'] ?></td>
                      <td class="<?= $item['hien_thi'] == 1 ? 'text-success' : 'text-warning' ?> "><?= $item['hien_thi'] == 1 ? 'Đã đăng' : 'Đang chờ duyệt' ?></td>
                      <td><a href="<?= base_url() ?>xem-tin-da-dang.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>">xem</a></td>

                    </tr>
                  <?php endforeach; ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end -col-md-9 -->

    </div>
  </div>
  <?php  include 'layouts/footer.php';?>
</body>
</html>
