// lat: vi? ?�?
// lng: kinh ?�?
// 21.007878494498556, 105.82484825300024
console.log('map.js loaded');
function initMap() {
  const mapElem = document.getElementById("map");
    const myLatlng = { lat: Number(mapElem.dataset.lat), lng: Number(mapElem.dataset.lng) };
    // const myLatlng = { lat: 21.007618284253272, lng: 105.82490175962448 };
    if(!myLatlng.lat || !myLatlng.lng) {
      myLatlng = { lat: 21.007618284253272, lng: 105.82490175962448 };
    }
    console.log('myLatlng', myLatlng);
    const map = new google.maps.Map(
      mapElem,
        {
            zoom: 50,
            center: myLatlng,
            mapTypeId: 'satellite',
            // mapTypeId: 'terrain',
        }
    );

    const image =
        "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png";
    const beachMarker = new google.maps.Marker({
        position: myLatlng,
        map,
        icon: image,
    });
    let infoWindow = new google.maps.InfoWindow({
        content: "Click the map to get Lat/Lng!",
        position: myLatlng,
      });
    map.addListener("click", (mapsMouseEvent) => {
        // Close the current InfoWindow.
        infoWindow.close();
    
        // Create a new InfoWindow.
        infoWindow = new google.maps.InfoWindow({
          position: mapsMouseEvent.latLng,
        });
        infoWindow.setContent(
          JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
        );
        infoWindow.open(map);
      });
}


window.initMap = initMap;