<?php include "autoload/autoload.php" ?>
<?php include 'layouts/head.php'; ?>

<body>
  <?php include 'layouts/header-top.php'; ?>

  <?php
  $phongtro = $db->query("SELECT * FROM nhatro WHERE hien_thi = 1 ORDER BY ma_nha_tro DESC LIMIT 9");
  $provinces = $db->fetchAll('province');
  ?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php include 'layouts/header-nav.php'; ?>
  <div class="slider hidden-xs hidden-sm">
    <img class="img-responsive" src="<?= base_url() ?>/public/images/bitnami.png" alt="">
    <div class="pt_container">
      <div class="slider-title">
        <h1>Chính chủ, Miễn phí, Tiện lợi</h1>
      </div>
      <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 search-bar">
        <form class="form" action="tim-kiem.php" method="post">
          <div class="group-arena pull-left">
            <div class="group-title">Chọn khu vực</div>
            <input class="ip-room-where" type="text" name="dia_chi" placeholder="Bạn muốn tìm phòng ở đâu ?">
            <a class="btn btn-select btn-select-light btn-select-city">
              <input type="hidden" class="btn-select-input" id="province" name="province" value="">
              <span class="btn-select-value">Thành Phố</span>
              <span class="btn-select-arrow fa fa-angle-down"></span>
              <ul style="display: none;">
                <?php foreach($provinces as $item) :?>
                  <li class="province" value="<?= $item['id'] ?>"><?= $item['_name'] ?></li>
                <?php endforeach; ?>
              </ul>
            </a>
            <a class="btn btn-select btn-select-light btn-select-city">
              <input type="hidden" class="btn-select-input" name="district" value="">
              <span class="btn-select-value">Quận</span>
              <span class="btn-select-arrow fa fa-angle-down"></span>
              <ul id="district" style="display: none;">
              </ul>
            </a>

          </div>

          <div class="group-type pull-left">
            <div class="group-title">Chọn loại phòng</div>
            <div class="radio radio-danger">
              <ul class="type-list-item">
              <?php foreach($categories as $item) :?>
                <li>
                  <input type="radio" name="danh_muc" id="<?= $item['id'] ?>" value="<?= $item['id'] ?>">
                  <label for="1"><?= $item['ten_danh_muc'] ?></label>
                </li>
              <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <div class="group-price pull-left">
            <div class="group-title">Chọn khoảng giá</div>

            <div class="extra-controls">
              <div class="form-group input-price">
                <div class="price-from">
                  <div class="input-group">
                    <input type="text" name="giatu" maxlength="4" value="0" class="inp js-from form-control">
                    <span class="input-group-addon">Triệu</span>
                  </div>
                </div>
                <span class="input-price-title">Đến</span>
                <div class="price-to">
                  <div class="input-group">
                    <input type="text" name="giaden" maxlength="4" value="50" class="inp js-to form-control">
                    <span class="input-group-addon">Triệu</span>
                  </div>
                </div>
              </div>
            </div>

            <!-- <div class="pt-ip-slide-price">
                        <input class="ip-room-where ip-room-price-start" type="text" placeholder="Giá Từ">
                        <input class="ip-room-where ip-room-price-end" type="text" placeholder="Giá Đến">
                        <i class="fa fa-arrows-h"></i>
                    </div> -->
          </div>
          <div class="clearfix"></div>
          <div class="group-btn-submit">
            <button type="submit" class="btn btn-slide-search"><span class="fa fa-search"></span>Tìm kiếm</button>
            <!-- <button type="submit" class="btn btn-advance-search">Tìm Nâng Cao</button> -->
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- end header nav -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <div class="room-list-category">
              <div class="menu-tab">
                <div class="menu-tab-title"><a href=""><span>Tin</span> Mới nhất <i class="fa fa-angle-right"></i></a></div>
              </div>
              <br><br><br>
              <!-- end menu-tab -->
              <div class="tab-content">

                <?php foreach ($phongtro as $item) : ?>
                  <div class="room-item">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="block-room-item-title">
                          <a href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>" style="text-transform: uppercase;"><?= $item['tieu_de'] ?></a>
                        </div>
                        <div class="col-md-3 room-item-thumbnail">
                          <div class="row">
                            <a class="col-md-12 thumbnail room-item-main-img" href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>">
                              <img class="img-reponsive" src="<?php base_url() ?>public/uploads/phongtro/<?= $item['hinh_anh'] ?>" alt="" style="height:150px; width:100%">
                            </a>

                          </div>
                        </div>
                        <div class="col-md-9 room-item-i">
                          <div class="block-room-item-address">
                            <span>Địa chỉ: </span>
                            <a>
                              <?= $item['dia_chi'] ?>
                            </a>
                          </div>
                          <div class="block-room-item-info">
                            <div class="pull-left">
                              <span>Diện tích: </span><a><?= $item['dien_tich'] ?> m <sup>2</sup></a>
                            </div>
                            <div class="pull-right item-info-date">
                              <a>
                                <?php echo date('d/m/Y', strtotime($item['thoi_gian_dang'])); ?>
                              </a>
                            </div>
                            <div class="item-info-vs block-room-item-price">
                              <span>Gía nước: </span><a><?= number_format($item['gia_nuoc'], 0, ',', '.') ?> Đ/tháng</a>
                              <span style="margin-left:25px">Gía điện: </span><a><?= number_format($item['gia_dien'], 0, ',', '.') ?> Đ/tháng</a>

                            </div>

                          </div>
                          <div class="block-room-item-price">
                            <span>Giá Phòng: </span><a><?= number_format($item['gia_phong'], 0, ',', '.') ?> Đ/tháng</a>
                          </div>

                          <div class="block-room-item-btn">
                            <a class="btn btn-room-detail" href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>">Xem chi tiết</a>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                <?php endforeach; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end -col-md-9 -->

    </div>
  </div>
  <?php include 'layouts/footer.php'; ?>
</body>

</html>

<script>
  var html = '';
  let result = [];
  $.each(result, function(key, item) {
    var value = item['id'];
    var name = item['name'];
    html += '<li value="' + value + '">' + name + '</li>';
  });
  $('#district').html(html);
  $("select").multipleSelect({
    width: 265,
    multiple: true,
    multipleWidth: 132,
    selectAll: false,
    placeholder: "Chọn Quận Huyện",
    // position: 'top',
    maxHeight: 400
  });
  $('body').on('click', '.province', function(e) {
    var key = $(this).val();
    var table = 'district';
    const baseUrl = '<?= base_url() ?>';
    $.ajax({
      url:  baseUrl + 'api/district.php',
      type: 'get',
      data: {
        id: key,
      },
      dataType: 'json',
      success: function(result) {
        console.log(111,result)
        result
        var html = '';
        $.each(result, function(key, item) {
          var value = item['id'];
          var name = item['_name'];
          html += '<li value="' + value + '">' + name + '</li>';
        });
        $('#district').html(html);
        $("select").multipleSelect({
          width: 265,
          multiple: true,
          multipleWidth: 132,
          selectAll: false,
          placeholder: "Chọn Quận Huyện",
          // position: 'top',
          maxHeight: 400
        });
      }
    });
  });
</script>

<script>
$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.val();
            var html = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(html);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});
</script>