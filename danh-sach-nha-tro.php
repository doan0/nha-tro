<?php include "autoload/autoload.php" ?>
<?php  include 'layouts/head.php';?>
<body>
  <?php  include 'layouts/header-top.php';?>

  <!-- in danh sach nha tro va phan trang -->
  <?php
  if (isset($_GET['p'])) {
    $p = $_GET['p'];
  } else {
    $p = 1;
  }
  $danhmuc = $_GET['danhmuc'];

  $sql = "SELECT * FROM nhatro WHERE hien_thi = 1 AND (id_danh_muc = $danhmuc OR id_danh_muc = 0) ORDER BY ma_nha_tro DESC";
    $total = $db->count_sql($sql);

  $data = $db->fetchJones("nhatro", $sql, $total, $p, 4, true);
  $total_page = $data['page'];
  unset($data['page']);
  ?>
  <!-- end header-top -->
  <div class="clearfix"></div>
  <?php  include 'layouts/header-nav.php';?>
  <!-- end header nav -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="col-md-12 room-main-content">
          <div class="tabbable-panel">
            <div class="room-list-category">
              <div class="menu-tab">
                <div class="menu-tab-title"><a href=""><span>Danh sách phòng trọ</span></a></div>
              </div>
              <br><br><br>
              <!-- end menu-tab -->
              <div class="tab-content">

                <?php foreach($data as $item) :?>
                <div class="room-item">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="block-room-item-title">
                        <a href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>" style="text-transform: uppercase;"><?= $item['tieu_de'] ?></a>
                      </div>
                      <div class="col-md-3 room-item-thumbnail">
                        <div class="row">
                          <a class="col-md-12 thumbnail room-item-main-img" href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>">
                            <img class="img-reponsive" src="<?php base_url() ?>public/uploads/phongtro/<?= $item['hinh_anh'] ?>" alt="" style="height:150px; width:100%">
                          </a>

                        </div>
                      </div>
                      <div class="col-md-9 room-item-i">
                        <div class="block-room-item-address">
                          <span>Địa chỉ: </span>
                          <a>
                            <?= $item['dia_chi'] ?>
                          </a>
                          </div>
                          <div class="block-room-item-info">
                            <div class="pull-left">
                              <span>Diện tích: </span><a><?= $item['dien_tich'] ?> m <sup>2</sup></a>
                            </div>
                            <div class="pull-right item-info-date">
                              <a>
                                <?php echo date('d/m/Y', strtotime($item['thoi_gian_dang'])); ?>
                              </a>
                              </div>
                              <div class="item-info-vs block-room-item-price">
                                <span>Gía nước: </span><a><?= number_format($item['gia_nuoc'], 0, ',','.') ?> Đ/tháng</a>
                                <span style="margin-left:25px">Gía điện: </span><a><?= number_format($item['gia_dien'], 0, ',','.') ?> Đ/tháng</a>

                              </div>

                            </div>
                            <div class="block-room-item-price">
                              <span>Giá Phòng: </span><a><?= number_format($item['gia_phong'], 0, ',','.') ?> Đ/tháng</a>
                            </div>

                          <div class="block-room-item-btn">
                            <a class="btn btn-room-detail" href="chi-tiet.php?ma_nha_tro=<?= $item['ma_nha_tro'] ?>">Xem chi tiết</a>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end -col-md-9 -->

      </div>
      <div style="text-align: center">
        <nav aria-label="...">
          <ul class="pagination">

            <?php for ($i = 1; $i <= $total_page; $i++): ?>
              <?php
              if (isset($_GET['p']) && $_GET['p'] == $i) {
                $active = "active";
              } else {
                $active = "";
              }

              ?>
              <li class="page-item <?php echo $active ?>">
                <a class="page-link" href="?danhmuc=<?= $danhmuc ?>&p=<?= $i ?>"><?= $i ?></a>
              </li>
            <?php endfor ?>

          </ul>
        </nav>
      </div>
    </div>

    <?php  include 'layouts/footer.php';?>
    <?php
    if (!isset($_GET['p'])) {
      echo '<script>
      $(".pagination li:nth-child(1)").addClass("active");
      </script>';
    } ?>
  </body>
  </html>
